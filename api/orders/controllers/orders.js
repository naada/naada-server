'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */



module.exports = {
    async  create (ctx)  {
    const { body } = ctx.request;
    await strapi.services.orders.create({userID:body.userID,
        price:body.price,
        tax:body.tax,
        orderId:body.orderId})

        // await strapi.services.orderItems.create({"productName":"Testing 3", "price":59.03,"tax":5,"status":"Active","OrderId":"orderId_12345677","qty":2})
}};
